# [acme-dns-01-ovh](https://git.rootprojects.org/root/acme-dns-01-ovh.js) | a [Root](https://rootprojects.org) project

OVH DNS + Let's Encrypt for Node.js

This handles ACME dns-01 challenges, compatible with ACME.js and Greenlock.js.
Passes [acme-dns-01-test](https://git.rootprojects.org/root/acme-dns-01-test.js).

# Install

```bash
npm install --save acme-dns-01-ovh@3.x
```

# Usage

First you need to create API keys on the OVH portal :

Go to the page https://api.ovh.com/createToken/index.cgi?GET=/domain/zone&PUT=/domain/zone&POST=/domain/zone&DELETE=/domain/zone

Then you create an instance with your API keys 

```js
var dns01 = require('acme-dns-01-ovh').create({
	applicationKey: 'xxxx', // default
	applicationSecret: 'xxxx',
	consumerKey: 'xxxx',
	region: 'ovh-eu', // one of ovh-eu, ovh-ca, kimsufi-eu, kimsufi-ca, soyoustart-eu, soyoustart-ca, runabove-ca
});
```

Then you can use it with any compatible ACME module,
such as Greenlock.js or ACME.js.

### Greenlock.js

```js
var Greenlock = require('greenlock-express');
var greenlock = Greenlock.create({
	challenges: {
		'dns-01': dns01
		// ...
	}
});
```

See [Greenlock™ Express](https://git.rootprojects.org/root/greenlock-express.js)
and/or [Greenlock.js](https://git.rootprojects.org/root/greenlock.js) documentation for more details.

### ACME.js

```js
// TODO
```

See the [ACME.js](https://git.rootprojects.org/root/acme-v2.js) for more details.

### Build your own

```js
dns01
	.set({
		identifier: { value: 'foo.example.com' },
		wildcard: false,
		dnsHost: '_acme-challenge.foo.example.com',
		dnsAuthorization: 'xxx_secret_xxx'
	})
	.then(function() {
		console.log('TXT record set');
	})
	.catch(function() {
		console.log('Failed to set TXT record');
	});
```

See [acme-dns-01-test](https://git.rootprojects.org/root/acme-dns-01-test.js)
for more implementation details.

# Tests

```bash
# node ./test.js domain-zone api-key
node ./test.js example.com xxxxxx
```
